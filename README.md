# Jetbarins-observer-pattern

Given classes and interface are the components of the Observer pattern.
RockstarGames is the concrete implementation of the  Observable and  Gamer is the concrete implementation of the Observer.
Implement RockstarGames methods to notify the gamers when a new game is released.
Please, do not change the provided code of the classes.

Sample Input 1:
Red Dead Redemption

Sample Output 1:
Notification for gamer : Garry Rose
Garry Rose says : "Oh, Rockstar releases new game Red Dead Redemption !"
Notification for gamer : Peter Johnston
Peter Johnston says : "Oh, Rockstar releases new game Red Dead Redemption !"
Notification for gamer : Helen Jack
Helen Jack says : "Oh, Rockstar releases new game Red Dead Redemption !"

