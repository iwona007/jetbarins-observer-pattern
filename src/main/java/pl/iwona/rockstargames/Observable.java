package pl.iwona.rockstargames;
/**
 * Observable interface
 **/
public interface Observable {
    void addObserver(Observer Observer);

    void removeObserver(Observer observer);

   void notifyObservers();

}
