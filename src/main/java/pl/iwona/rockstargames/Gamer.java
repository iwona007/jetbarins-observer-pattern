package pl.iwona.rockstargames;

import java.util.HashSet;
import java.util.Set;

/**
 * Concrete observer - Gamer
 **/
public class Gamer implements Observer{
    private String name;

    private Set<String> games = new HashSet<>();

    public Gamer(String name) {
        this.name = name;
    }


    @Override
    public void update(String game) {
    buyGame(game);
    }

    public void buyGame(String game) {
        System.out.println(name + " says : \"Oh, Rockstar releases new game " + game + " !\"");
        games.add(game);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
