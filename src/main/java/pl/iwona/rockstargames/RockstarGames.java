package pl.iwona.rockstargames;

import java.util.ArrayList;
import java.util.List;

/**
 * Concrete Observable - Rockstar Games
 **/
public class RockstarGames implements Observable {

    public String releaseGame;
    List<Observer> observerList = new ArrayList<>();

    public void release(String releaseGame) {
        this.releaseGame = releaseGame;
        notifyObservers();
    }


    @Override
    public void addObserver(Observer observer) {
        observerList.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observerList) {
            System.out.println("Notification for gamer : " + observer);
            observer.update(releaseGame);
        }
    }


}
