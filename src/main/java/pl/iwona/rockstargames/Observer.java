package pl.iwona.rockstargames;

/**
 * Observer interface
 **/
public interface Observer {

    void update(String domain);
}
