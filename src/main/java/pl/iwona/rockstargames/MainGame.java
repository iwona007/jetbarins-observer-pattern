package pl.iwona.rockstargames;

import java.util.Scanner;

public class MainGame {
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        String game = null;

        RockstarGames rockstarGames = new RockstarGames();

        Gamer gamer = new Gamer("Garry Rose");
        Gamer gamer2 = new Gamer ("Peter Johnston");
        Gamer gamer3 = new Gamer ("Helen Jack");
         rockstarGames.addObserver(gamer);
         rockstarGames.addObserver(gamer2);
         rockstarGames.addObserver(gamer3);

         game = scanner.nextLine();
         rockstarGames.release(game);

         scanner.close();
    }
}
